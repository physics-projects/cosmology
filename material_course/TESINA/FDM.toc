\select@language {english}
\contentsline {chapter}{\numberline {1}$\Lambda $CDM on small scales}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Controversies}{7}{section.1.1}
\contentsline {subparagraph}{The cusp-core problem}{7}{subparagraph*.4}
\contentsline {section}{\numberline {1.2}Possible solutions}{10}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Baryonic physics}{10}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Warm dark matter}{10}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Self Interacting dark matter}{10}{subsection.1.2.3}
\contentsline {chapter}{\numberline {2}Physics of FDM}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Ultralight scalars in cosmology}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}FDM as a superfluid}{17}{section.2.2}
\contentsline {section}{\numberline {2.3}Cosmological perturbations and power spectrum}{19}{section.2.3}
\contentsline {section}{\numberline {2.4}Solitons and Halo Density Profiles}{22}{section.2.4}
\contentsline {section}{\numberline {2.5}Simulations with FDM}{26}{section.2.5}
\contentsline {chapter}{\numberline {3}Observational constraints on FDM}{31}{chapter.3}
\contentsline {section}{\numberline {3.1}FDM astrophysics in nearby galaxies}{31}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Relaxation of FDM dominated systems}{32}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}The missing satellite problem and too-big-to-fail problem}{32}{subsection.3.1.2}
\contentsline {subsubsection}{Lower bound on FDM halo masses}{33}{subsubsection*.17}
\contentsline {subsubsection}{Subhalos tidal disruption}{33}{subsubsection*.18}
\contentsline {section}{\numberline {3.2}High-redshift constraints on FDM}{34}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}FDM and galaxy formation}{34}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Lyman-$\alpha $ forest constraints }{35}{subsection.3.2.2}
\contentsline {subsubsection}{FDM + CDM perturbations and power spectrum}{35}{subsubsection*.19}
\contentsline {subsubsection}{Lyman-$\alpha $ and the missing satellite}{37}{subsubsection*.21}
\contentsline {subsubsection}{Lyman-$\alpha $ and the initial mislignement}{38}{subsubsection*.23}
\contentsline {subsubsection}{The importance of modified dynamics}{38}{subsubsection*.25}
\contentsline {section}{\numberline {3.3}Perspectives on ULA detection }{39}{section.3.3}
\contentsline {subsubsection}{Pressure oscillations and PTA}{40}{subsubsection*.26}
